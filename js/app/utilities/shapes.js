define(function(require){
  var Graphics = require('graphics');
  
  var Shapes = {
    Sphere: function (center, radius, material) {
      this.position = center;
      this.radius = radius;
      this.material = material;

      this.collide = function(ray) {
        var lc = ray.d.dot(this.position);
        var c2 = this.position.dot(this.position);
        var r2 = this.radius * this.radius;

        var disc = (lc * lc) - c2 + r2;

        rayPosition = null;
        if (disc < 0) { return new Graphics.Hit(null, null, null, null); }
        else if (disc === 0) { rayPosition = lc; }
        else { rayPosition = lc - Math.sqrt(disc); } //Cheating and assuming the sphere is entirely in front of the camera.

        var point = ray.pointAt(rayPosition);
        var surfNormal = point.sub(this.position).normalize();

        return new Graphics.Hit(rayPosition, point, surfNormal, this);
      };
    }
  };
  
  return Shapes;
});