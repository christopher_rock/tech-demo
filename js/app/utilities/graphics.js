define(function(require) {
  var MathUtils = require('math_utils');
  
  var Graphics = {
    Material: function (diffuseColorV, ambientColorV) {
      this.dColor = diffuseColorV;
      this.aColor = ambientColorV;
    },

    Light: function (position, material, attenuation) {
      this.position = position;
      this.material = material;
      this.attenuation = attenuation;

      this.check = function(point, normal, material) {
        var dirVector = this.position.sub(point).normalize();
        var intensity = dirVector.dot(normal);
        intensity = Math.max(intensity, 0);

        var theColor = material.dColor.scalarMult(intensity);
        theColor = theColor.add(material.aColor);
        theColor.clamp();
        return theColor;
      };
    },

    Camera: function (width, height) {
      this.width = width;
      this.height = height;
      this.pos = new MathUtils.Vector(0, 0, 0);
      this.facing = new MathUtils.Vector(0, 0, -1);
      this.up = new MathUtils.Vector(0, 1, 0);
      this.fov = Math.PI / 4;
      this.aspect = width / height;
      this.near = 1.0;

      this._calculatePlaneParameters = function() {
        this.planeHeight = 2 * this.near * Math.tan(this.fov / 2);
        this.planeWidth = this.planeHeight * this.aspect;
        this.perPixel = this.planeWidth / this.width;
        this.topLeft = new MathUtils.Vector(-this.planeWidth / 2, this.planeHeight / 2, -this.near);
      };

      this.getRayForPixel = function(x, y) {
        var newX = this.topLeft.x + (this.perPixel * x);
        var newY = this.topLeft.y - (this.perPixel * y);
        var newZ = -this.near;

        return new MathUtils.Ray(this.pos, new MathUtils.Vector(newX, newY, newZ).sub(this.pos));
      };

      this._calculatePlaneParameters();
    },
    
    Hit: function (pointAlongRay, globalPoint, surfaceNormal, intersectedObj) {
      this.pointAlongRay = pointAlongRay;
      this.globalPoint = globalPoint;
      this.surfaceNormal = surfaceNormal;
      this.intersectedObj = intersectedObj;
    }
  };
  
  return Graphics;
});