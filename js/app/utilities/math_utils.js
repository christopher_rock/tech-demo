define(function(require) {
  var MathUtils = {
    Color: function (r, g, b, a) {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;

      this.add = function(other) {
        var r = this.r + other.r;
        var g = this.g + other.g;
        var b = this.b + other.b;
        var a = this.a + other.a;
        return new MathUtils.Color(r, g, b, a);
      };

      this.sub = function(other) {
        var r = this.r - other.r;
        var g = this.g - other.g;
        var b = this.b - other.b;
        var a = this.a - other.a;
        return new MathUtils.Color(r, g, b, a);
      };

      this.scalarMult = function(value) {
        var r = this.r * value;
        var g = this.g * value;
        var b = this.b * value;
        var a = this.a * value;
        return new MathUtils.Color(r, g, b, a);
      };

      this.scalarDiv = function(value) {
        var r = this.r / value;
        var g = this.g / value;
        var b = this.b / value;
        var a = this.a / value;
        return new MathUtils.Color(r, g, b, a);
      };

      this.clamp = function() {
        this.r = Math.min(this.r, 1);
        this.g = Math.min(this.g, 1);
        this.b = Math.min(this.b, 1);
        this.a = Math.min(this.a, 1);

        this.r = Math.max(this.r, 0);
        this.g = Math.max(this.g, 0);
        this.b = Math.max(this.b, 0);
        this.a = Math.max(this.a, 0);

        return this;
      };
    },
    
    Vector: function (x, y, z) {
      this.x = x;
      this.y = y;
      this.z = z;

      this.add = function(other) {
        var x = this.x + other.x;
        var y = this.y + other.y;
        var z = this.z + other.z;
        return new MathUtils.Vector(x, y, z);
      };

      this.sub = function(other) {
        var x = this.x - other.x;
        var y = this.y - other.y;
        var z = this.z - other.z;
        return new MathUtils.Vector(x, y, z);
      };

      this.dot = function(other) {
        return (this.x * other.x) + (this.y * other.y) + (this.z * other.z);
      };

      this.cross = function(other) {

      };

      this.normalize = function() {
        var length = Math.sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
        this.x = this.x / length;
        this.y = this.y / length;
        this.z = this.z / length;

        return this;
      };

      this.negate = function() {
        return new MathUtils.Vector(-x, -y, -z);
      };

      this.scalarMult = function(value) {
        var x = this.x * value;
        var y = this.y * value;
        var z = this.z * value;
        return new MathUtils.Vector(x, y, z);
      };

      this.scalarDiv = function(value) {
        var x = this.x / value;
        var y = this.y / value;
        var z = this.z / value;
        return new MathUtils.Vector(x, y, z);
      };
    },
    
    Ray: function (origin, direction) {
        this.o = origin;
        this.d = direction.normalize();

        this.move = function(trans) {
            this.o = this.o.add(trans);
        };

        this.pointAt = function(t) {
            return this.d.scalarMult(t).add(this.o);
        };
    }
  };
  
  return MathUtils;
});