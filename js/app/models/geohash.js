define(function(require) {
  var _ = require('underscore'),
      Backbone = require('backbone'),
      moment = require('moment'),
      mtz = require('moment-timezone'),
      mtzd = require('moment-timezone-data'),
      md5 = require('md5');

  var GeohashModel = Backbone.Model.extend({
    urlRoot: '/dow',
    url: function() {
      return this.urlRoot + '/' + this.get('date');
    },
    
    defaults: {
      "hashType": "local",
      "enteredLocation": "Fairfax, VA",
      "coords": {},
      "locationServicesAvailable": 'false',
      "locationServicesEnabled": false,
      "dow": "10458.68",
      "date": moment().tz('America/New_York').format('YYYY-MM-DD')
    },
    
    validate: function(attrs, opts) {
      var validTypes = {'local': null, 'global': null};
      var validServices = {'false': null, 'browser': null, 'google': null};
      if (!moment(attrs.date, 'YYYY-MM-DD', true).isValid())
        return "Invalid date";
      if (_.isNaN(parseFloat(attrs.dow)))
        return "Invalid value for Dow";
      if (!(attrs.locationServicesAvailable in validServices))
        return "Invalid Location Service";
      if (!(attrs.hashType in validTypes))
        return "Invalid Hash Type";
    },
    
    getDecimals: function() {
      if (this.isValid()) {
        var toHash = this.get('date') + '-' + this.get('dow');
        console.log(md5.md5(toHash));
        var vals = md5.md5ints(toHash);
        
        var latDecimal = 0.0, longDecimal = 0.0, i = 0;
        var val = null, j = 0, hexVal = 0, power = 0;
        
        // Return from md5ints is an array of 4 32-bit integers representing
        // the value of the md5 hash. The first is the most significant
        // and the last is the least. The first two are used to compute
        // the decimal portion of the latitude graticule and the last
        // two of the longitude.
        for (; i < 2; i++) {
          val = vals[i];
          j = 0;
          // Javascript is little endian so we'll start at the rightmost
          // byte considering it the most significant. Processing
          // byte by byte for a good balance of eliminating rounding
          // errors and doing less processing.
          for (; j < 4; j++) {
            hexVal = (val >> (j * 8)) & 0x00FF;
            power = i * 4 + (j + 1);
            latDecimal += hexVal / Math.pow(256, power);
          }
        }
        
        // Same as above but for the longitude on the 3rd and 4th items.
        for (; i < 4; i++) {
          val = vals[i];
          j = 0;
          for (; j < 4; j++) {
            hexVal = (val >> (j * 8)) & 0x00FF;
            power = (i - 2) * 4 + (j + 1);
            longDecimal += hexVal / Math.pow(256, power);
          }
        }
        
        return {
          'latitude': latDecimal,
          'longitude': longDecimal
        };
      }
    }
  });

  return GeohashModel;
});
