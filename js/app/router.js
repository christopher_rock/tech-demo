define(function (require) {
  var $ = require('jquery'),
      Backbone = require('backbone'),
      ga = require('google-analytics');

  var router = Backbone.Router.extend({
    routes: {
      "": "other",
      "game/:name": "game",
      "demo/:name": "demo",
      "demo/geohash": "geohash",
      "demo/geohash?*querystring": "geohash",
      ":name": "other"
    },

    execute: function(callback, args) {
      var hash = window.location.hash;
      var page = hash.replace('#', '/');
      ga('send', 'pageview', {
        'page': page
      });
      $('.popover').remove();
      if (callback) callback.apply(this, args);
    },

    other: function(name) {
      if (!name) {
        name = "home";
      }
      require([name + '_view'], function(TheView) {
        var view = new TheView({el: $('#content')});
        view.render();
        $('#menu-' + name).addClass('active');
      });
    },

    demo: function(name) {
      require([name + '_view'], function(TheView) {
        var view = new TheView({el: $('#content')});
        view.render();
        $('#menu-demos-dropdown').addClass('active');
      });
    },

    geohash: function(querystring) {
      require(['geohash_view'], function(TheView) {
        var view = new TheView({el: $('#content')});
        view.render(querystring);
        $('#menu-demos-dropdown').addClass('active');
      });
    },

    game: function(name) {
      require(['flash_game_view'], function(FlashGameView) {
        var view = new FlashGameView({el: $('#content')});
        view.render(name);
        $('#menu-games-dropdown').addClass('active');
      });
    }
  });

  return router;
});
