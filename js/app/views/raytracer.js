define(function(require){
  var $ = require('jquery'),
      Backbone = require('backbone'),
      MathUtils = require('math_utils'),
      Graphics = require('graphics'),
      Shapes = require('shapes'),
      raytracerTemplate = require('text!/js/app/templates/raytracer_template.tmpl');
  
  var RaytracerView = Backbone.View.extend({
    events: {
      "click #render-button": "beginRender"
    },
    
    render: function() {
      this.$el.html(_.template(raytracerTemplate));
            
      return this;
    },
    
    beginRender: function() {
      $('#render-button').button('loading');
      
      var theCanvas = $('#raytracer-canvas');
      var height = theCanvas[0].height;
      var width = theCanvas[0].width;
      var theContext = theCanvas[0].getContext('2d');

      var imageData = theContext.createImageData(width, height);
      var start_time = new Date();
      var stats = $('#raytracer-stats');

      var intID = window.setInterval(function() {
        theContext.putImageData(imageData, 0, 0);
        var time = new Date() - start_time;
        stats.text("Rendering: " + time + " ms.");
      }, 1000 / 30.0);

      this.doScan(imageData, width, height);

      window.clearInterval(intID);
      theContext.putImageData(imageData, 0, 0);
      var time = new Date() - start_time;
      stats.text("Render took " + time + " ms. Instantaneous framerate of " + 1000 / time + " FPS");
      
      $('#render-button').button('reset');
    },
    
    doScan: function (imageData, width, height) {
      var materials = [new Graphics.Material(new MathUtils.Color(1, 0, 0, 1), new MathUtils.Color(0.1, 0, 0, 1)),
                       new Graphics.Material(new MathUtils.Color(0, 1, 0, 1), new MathUtils.Color(0, 0.1, 0, 1)),
                       new Graphics.Material(new MathUtils.Color(1, 1, 1, 1), new MathUtils.Color(0.1, 0.1, 0.1, 1))];
      
      var camera = new Graphics.Camera(width, height);
      var things = [new Shapes.Sphere(new MathUtils.Vector(0, 0, -5), 2, materials[0]),
                    new Shapes.Sphere(new MathUtils.Vector(1, 0.5, -3.5), 0.25, materials[1])];
      var lights = [new Graphics.Light(new MathUtils.Vector(-3, 0, -3), materials[2], 1)];
      
      var circleVals = {};
      for (var j = 0; j < height; j++) {
        for (var i = 0; i < width; i++) {
          var theRay = camera.getRayForPixel(i, j);
          var pixelColor = null;
          var hits = [];
          var k = 0;
          for (k = 0; k < things.length; k++) {
            var thing = things[k];
            var hit = thing.collide(theRay);
            if (hit.pointAlongRay !== null)
              hits.push(hit);
          }
          var indPos = (j * width * 4) + (i * 4);
          imageData.data[indPos + 3] = 255;
          
          if (hits.length === 0) {
            imageData.data[indPos] = 0;
            imageData.data[indPos + 1] = 0;
            imageData.data[indPos + 2] = 0;
            continue;
          }
          
          var theHit = null;
          for (k = 0; k < hits.length; k ++) {
            if (theHit === null || hits[k].pointAlongRay < theHit.pointAlongRay)
              theHit = hits[k];
          }
          
          for (k = 0; k < lights.length; k ++) {
            var newColor = lights[k].check(theHit.globalPoint, theHit.surfaceNormal, theHit.intersectedObj.material);
            if (pixelColor === null)
              pixelColor = newColor;
            else
              pixelColor = pixelColor.add(newColor);
          }
          
          imageData.data[indPos] = pixelColor.r * 255;
          imageData.data[indPos + 1] = pixelColor.g * 255;
          imageData.data[indPos + 2] = pixelColor.b * 255;
        }
    }
    }
  });
  
  return RaytracerView;
});