define(function(require){
  var $ = require('jquery'),
      _ = require('underscore'),
      Backbone = require('backbone'),
      homeTemplate = require('text!/js/app/templates/home_template.tmpl');
  
  var HomeView = Backbone.View.extend({
    events: {
      'click .jumbotron a': window.trackOutboundLink
    },
    
    render: function() {
      var items = [
        {
          url: "/img/screenshots/raytracer-ss.png",
          link: "#raytracer",
          name: "Raytracer",
          desc: "A raytracer written in pure Javascript using Canvas for display"
        },
        {
          url: "/img/screenshots/pong-ss.png",
          link: "#game/pong",
          name: "Pong",
          desc: "Multiplayer pong game. Flash on the client side, C# and <a href='http://playerio.com/'>Player.io</a> server side."
        },
        {
          url: "/img/screenshots/noir-ss.png",
          link: "#game/noir",
          name: "Noir",
          desc: "A Flash game made for <a href='http://www.ludumdare.com'>Ludum Dare</a> 28 with a small team in 48 hours."
        },
      ];
      
      this.$el.html(_.template(homeTemplate, {screenshots: items}, {variable: 'data'}));
      
      return this;
    }
  });
  
  return HomeView;
});