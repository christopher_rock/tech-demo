define(function(require){
  var $ = require('jquery'),
      Backbone = require('backbone'),
      backboneForms = require('backbone-forms'),
      GMaps = require('async!https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false'),
      GeohashTemplate = require('text!/js/app/templates/geohash_template.tmpl'),
      GeohashModel = require('geohash_model');

  var GeohashView = Backbone.View.extend({
    model: new GeohashModel(),

    events: {
      "change .geohash-type-select input": "hashTypeChanged",
      "keyup #location-entry": "locationChanged",
      "click .location-services": "toggleLocationServices",
      "click .update-hash": "updateHash"
    },

    map: null,
    geoCoder: null,
    curMarker: null,

    render: function(querystring) {
      this.$el.html(_.template(GeohashTemplate, {dow: this.model.get('dow')}, {variable: 'data'}));

      var mapOptions = {
        zoom: 11,
        center: new google.maps.LatLng(38.80, -77.40)
      };
      this.map = new google.maps.Map($('.geohash-map-container')[0], mapOptions);
      this.checkForLocationServices();

      if (!Modernizr.touch)
        $('.has-popover').popover();

      this.model.on('change:dow', this.setDow);
      this.refreshDow();

      return this;
    },

    checkForLocationServices: function() {
      var that = this;
      if (navigator.geolocation) {
        console.log('Geolocation services provided by browser');
        this.model.set('locationServicesAvailable', 'browser');
        this.enableLocationServices();
      } else {
        console.log('Geolocation services not provided by browser. Falling back to google');
        require(['goog!search,1'], function() {
          if (google.clientLocation) {
            console.log('Geolocation services provided by Google');
            that.model.set('locationServicesAvailable', 'google');
            that.enableLocationServices();
          } else {
            console.log('Geolocation services not available');
            that.disableLocationServices();
          }
        });
      }
    },

    setDow: function(model, newDow) {
      $('#dow-value').val(newDow);
    },

    enableLocationServices: function() {
      $('.location-services').removeClass('disabled');
    },

    disableLocationServices: function() {
      $('.location-services').addClass('disabled');
      this.model.set('locationServicesAvailable', 'false');
    },

    hashTypeChanged: function(e) {
      var other = {'local': 'global', 'global': 'local'};
      var changedTo = $(e.target).val();
      this.model.set('hashType', changedTo);

      $('.' + changedTo + '-options').removeClass('hidden');
      $('.' + other[changedTo] + '-options').addClass('hidden');
    },

    locationChanged: function(e) {
      // When the user changes the value in the location box, if they've already clicked
      // the location services box, we want to make it clear to them that it is not still
      // the location given by the browser.
      if (this.model.get('locationServicesEnabled')) {
        $('.local-options .location-services').removeClass('loc-enabled').addClass('loc-disabled');
        this.model.set('locationServicesEnabled', false);
        $('div.input-group:has(#location-entry)').removeClass('has-error');
      }
    },

    toggleLocationServices: function(e) {
      var locationButton = $('.local-options .location-services');
      if (this.model.get('locationServicesEnabled')) {
        this.model.set('locationServicesEnabled', false);
        locationButton.removeClass('loc-enabled').addClass('loc-disabled');
      } else {
        this.getUserLocation();
      }
    },

    getUserLocation: function() {
      var that = this;
      // Get the location from the browser. We don't need anything especially current so just
      // get anything within the last hour.
      if (this.model.get('locationServicesAvailable') == 'browser') {
        navigator.geolocation.getCurrentPosition(function(position) {
          // Got a new position. Need to update everything.
          // Start with the entered position value
          var curPosition = position.coords.latitude.toFixed(4) + ' ' + position.coords.longitude.toFixed(4);
          $('.local-options #location-entry').val(curPosition);
          that.model.set('enteredLocation', curPosition);
          // Now set the coordinate values. This is the real meat.
          that.model.set('coords', {lat: position.coords.latitude, lng: position.coords.longitude});
          // Now update the interface to show the user we have location.
          that.model.set('locationServicesEnabled', true);
          $('div.input-group:has(#location-entry)').removeClass('has-error');
          $('.local-options .location-services').removeClass('loc-disabled').addClass('loc-enabled');
        },function(error) {
          // 1 means the user refused providing location. 2 means the location was not found
          // for whatever reason. 3 means it timed out so we can let the user try again.
          if (error.code === 1 || error.code === 2) {
            that.disableLocationServices();
            console.error("Location services not available");
          }
        }, {maximumAge:3600000});
      }
    },

    processLocation: function(callback) {
      if (this.model.get('locationServicesEnabled') && this.model.get('coords')) {
        // Geolocation coordinates were used. We should have everything we need.
        callback();
      } else {
        // Geolocation coordinates were not used. We need to do something
        // with the user's entered location.
        this.geoCoder = (this.geoCoder || new google.maps.Geocoder());
        var that = this;

        var location = $('.local-options #location-entry').val();
        this.model.set('enteredLocation', location);
        // Google is pretty damned forgiving. Just throw whatever in for address
        // and it'll probably come back ok.
        this.geoCoder.geocode({'address': location}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            var coords = results[0].geometry.location;
            that.model.set('coords', {lat: coords.lat(), lng: coords.lng()});
            callback();
          } else {
            $('div.input-group:has(#location-entry)').addClass('has-error');
          }
        });
      }
    },

    refreshDow: function() {
      $('.refresh-dow').button('loading');
      this.model.fetch({
        success: function(model, res, options) {
          $('.refresh-dow').button('reset');
        }
      });
    },

    updateHash: function(e) {
      var decimals = this.model.getDecimals();
      var that = this;
      var newLat, newLng;

      if (this.model.get('hashType') == 'local') {
        if (!this.model.get('locationServicesEnabled') && !$('.local-options #location-entry').val()){
          $('div.input-group:has(#location-entry)').addClass('has-error');
          return;
        }
        this.processLocation(function() {
          var userLoc = that.model.get('coords');

          newLat = userLoc.lat < 0 ? Math.ceil(userLoc.lat) : Math.floor(userLoc.lat);
          newLat = newLat < 0 ? newLat - decimals.latitude : newLat + decimals.latitude;

          newLng = userLoc.lng < 0 ? Math.ceil(userLoc.lng) : Math.floor(userLoc.lng);
          newLng = newLng < 0 ? newLng - decimals.longitude : newLng + decimals.longitude;
          var newCenter = {lat: newLat, lng: newLng};

          that.updateMap(newCenter)
        });
      } else {
        newLat = (180.0 * decimals.latitude) - 90.0;
        newLng = (360.0 * decimals.longitude) - 180.0;
        var newCenter = {lat: newLat, lng: newLng};

        this.updateMap(newCenter);
      }
    },

    updateMap: function(newCenter) {
      this.map.setCenter(newCenter);

      if (this.curMarker)
        this.curMarker.setMap(null);
      this.curMarker = new google.maps.Marker({
        position: newCenter,
        animation: google.maps.Animation.DROP,
        map: this.map,
        title: "Lat: " + newCenter.lat.toFixed(6) + " Lng: " + newCenter.lng.toFixed(6)
      });
    }
  });

  return GeohashView;
});
