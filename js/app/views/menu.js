define(function(require){
  var $ = require('jquery'),
      _ = require('underscore'),
      Backbone = require('backbone'),
      menuTemplate = require('text!/js/app/templates/menu_template.tmpl');

  var MenuView = Backbone.View.extend({
    events: {
      'click .navbar-brand': 'clickMenu',
      'click #menu-demos-dropdown>ul>li>a': 'clickMenu',
      'click #menu-games-dropdown>ul>li>a': 'clickMenu',
      'click .navbar-nav>li:not(.dropdown)>a': 'clickMenu'
    },

    render: function() {

      var demos = [
        {
          display: "geohash",
          link: "geohash",
        },
        {
          display: "raytracer",
          link: "raytracer",
        }
      ];
      var games = [
        {
          display: "multiplayer pong (flash)",
          link: "pong",
        },
        {
          display: "ld28 - noir (flash)",
          link: "noir",
        }
      ];

      $('#menu-demos-dropdown').append(_.template(menuTemplate, {items: demos, menuName: 'demo'}, {variable: 'data'}));
      $('#menu-games-dropdown').append(_.template(menuTemplate, {items: games, menuName: 'game'}, {variable: 'data'}));
      
      $('#menu-resume>a').on('click', window.trackOutboundLink);

      return this;
    },

    clickMenu: function() {
      $('ul.nav.navbar-nav li').removeClass('active');
      $('.navbar-collapse').removeClass('in');
    }
  });

  return MenuView;
});
