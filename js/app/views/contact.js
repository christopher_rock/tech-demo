define(function(require){
  var $ = require('jquery'),
      Backbone = require('backbone'),
      ContactTemplate = require('text!/js/app/templates/contact_template.tmpl');

  var ContactView = Backbone.View.extend({
    events: {
      'click .social-links>a': window.trackOutboundLink
    },
    
    render: function() {
      this.$el.html(_.template(ContactTemplate));

      return this;
    }
  });

  return ContactView;
});
