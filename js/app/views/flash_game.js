define(function(require){
  var $ = require('jquery'),
      _ = require('underscore'),
      Backbone = require('backbone'),
      swf = require('swfobject'),
      flashGameTemplate = require('text!/js/app/templates/flash_game_template.tmpl');

  var flashGameView = Backbone.View.extend({
    games: {
      'pong': {
        gameName: "Multiplayer Pong",
        flashvars: null,
        attributes: null,
        url: "http://asyncadventure.s3.amazonaws.com/network_pong_preloader.swf",
        width: "800px",
        height: "600px",
        description: "Flash multiplayer version of the classic game Pong. Client side developed in ActionScript 3 using Starling as a framework. Multiplayer functionality coded with Player.io services and C# on the server side.",
        credits: [{
          name: 'Chris Rock',
          responsibilities: 'Client- and server-side development'
        },{
          name: 'Ben Walker - <a href="http://garagecoder.com/">Garage Coder</a>',
          responsibilities: 'Client- and server-side development'
        }]
      },
      'noir': {
        gameName: "Noir LD28",
        flashvars: null,
        attributes: null,
        url: "http://asyncadventure.s3.amazonaws.com/noir.swf",
        width: "800px",
        height: "600px",
        description: "Flash game developed for <a href='http://ludumdare.com/'>Ludum Dare</a> 28 in 48 hours with a small team. Theme was \"You only get one\"",
        credits: [{
          name: 'Chris Rock',
          responsibilities: 'AS3 development, art'
        },{
          name: 'Ben Walker - <a href="http://garagecoder.com/">Garage Coder</a>',
          responsibilities: 'Design, AS3 development, art'
        },{
          name: 'Ryuno - <a href="https://soundcloud.com/ryunocore">Soundcloud</a>',
          responsibilities: 'Music and Sound effects'
        },{
          name: 'Erin Napolitano',
          responsibilities: 'Writing/Dialog'
        }]
      }
    },

    render: function(gameName) {
      var gameInfo = this.games[gameName];

      this.$el.html(_.template(flashGameTemplate, gameInfo, {variable: 'data'}));

      $(document).ready(function() {
        if (!gameInfo.flashvars)
          gameInfo.flashvars = {};
        if (!gameInfo.attributes)
          gameInfo.attributes = {};
        if (!gameInfo.attributes['class'])
          gameInfo.attributes['class'] = "center-block";
        else if (gameInfo.attributes['class'].indexOf("center-block") === -1)
          gameInfo.attributes['class'] += " center-block";
        var params = {
            menu: "false",
            scale: "noScale",
            allowFullscreen: "true",
            allowScriptAccess: "always",
            bgcolor: "",
            wmode: "direct" // can cause issues with FP settings & webcam
        };
        swf.embedSWF(
            gameInfo.url,
            "altContent", gameInfo.width, gameInfo.height, "10.0.0",
            "expressInstall.swf",
            gameInfo.flashvars, params, gameInfo.attributes);
      });

      return this;
    }
  });

  return flashGameView;
});
