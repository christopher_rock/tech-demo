define(function (require) {
  var phaser = require('phaser'),
      d3 = require('d3'),
      $ = require('jquery'),
      _ = require('underscore'),
      Bootstrap = require('bootstrap'),
      MenuView = require('menu_view'),
      ga = require('google-analytics');
  
  window.trackOutboundLink = function(e) {
    e.preventDefault();
    var url = $(e.currentTarget).attr('href');
    ga('send', 'event', 'outbound', 'click', url, {'hitCallback':function () {
        document.location = url;
      }
    });
  };
  
  function initialize() {
    var menuView = new MenuView({el: $('#menu>.container')});
    menuView.render();
    
    $('#footer .social-icon-sm').on('click', window.trackOutboundLink);
  }

  function buildSVGTest() {
    var width = $('.chart').width(),
        height = $('.chart').height();
    var n = 10.0,
        h = 50.0;
    var data = d3.range(0, n).map(function () { return Math.random() * h; });
    var x = d3.scale.linear()
              .domain([0, n-1])
              .range([0, width]);
    var y = d3.scale.linear()
              .domain([0, h])
              .range([height, 0]);
    var svg = d3.select('.chart')
                .append('svg');
    var g = svg.append('g').attr("clip-path", "url(#clip)");
    var line = d3.svg.line()
                .x(function(d, i) { return x(i); })
                .y(function(d, i) { return y(d); })
                .interpolate('basis');
    var path = g.append('path').datum(data).attr('d', line).attr('class', 'line');
  }

  // $(window).ready(buildSVGTest);
  $(window).ready(initialize);
});
