var require = {
    "baseUrl": "js/lib",
    "paths": {
      "app": "/js/app",
      
      //Models
      "geohash_model": "/js/app/models/geohash",
      
      // Utilities
      "graphics": "/js/app/utilities/graphics",
      "math_utils": "/js/app/utilities/math_utils",
      "shapes": "/js/app/utilities/shapes",
      
      // Views
      "menu_view": "/js/app/views/menu",
      "home_view": "/js/app/views/home",
      "geohash_view": "/js/app/views/geohash",
      "raytracer_view": "/js/app/views/raytracer",
      "flash_game_view": "/js/app/views/flash_game",
      "contact_view": "/js/app/views/contact"
    },
    "shim": {
      "bootstrap": {
        deps: ["jquery"]
      },
      'backbone': {
        deps: ['underscore', 'jquery'],
        exports: 'Backbone'
      },
      'underscore': {
        exports: '_'
      }
    }
};