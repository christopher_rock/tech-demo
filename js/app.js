// Load the main app module to start the app
requirejs(["app/main", "app/router", "backbone"], function(main, Router, backbone) {
  var router = new Router();
  
  backbone.history.start();
});