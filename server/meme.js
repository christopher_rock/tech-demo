var caption = require('caption'),
    sqlite3 = require('sqlite3').verbose();

var db = new sqlite3.Database('techDB.sqlite3');

exports.makeMeme = function(req, res){
    var imageUrl = req.param('imageUrl');
    var top = req.param('topText');
    var bottom = req.param('bottomText');

    if (!top && !bottom || !imageUrl) {
        res.status(400);
        res.send('imageUrl and (top and/or bottom caption) required');
    }

    var url = req.originalUrl;

    db.get('SELECT filename FROM pastMemes WHERE url = $url', {
        $url: url
    }, function(err, row) {
        if (err) {
            console.log('error in select');
            res.status(500);
            res.send(err);
            return;
        }
        console.log(row);
        if (row && row.filename) {
            console.log('Already made this meme. Resending');
            res.status(200);
            res.sendfile(row.filename);
            return;
        } else {
            console.log('Making a new meme');
            var opts = {
                outputFile: Date.now() + '.jpg',
            };
            if (top) opts.caption = top;
            if (bottom) opts.bottomCaption = bottom;

            caption.url(imageUrl, opts, function(err, captionedImage) {
                if (err) {
                    console.log('error in caption');
                    res.status(500);
                    res.send(err);
                    return;
                } else {
                    console.log('Memem is made, storing result in db');
                    db.run('INSERT INTO pastMemes (url, filename) VALUES ($url, $filename)', {
                        $url: url,
                        $filename: captionedImage
                    }, function(err){
                        if (err) {
                            console.log('error in insert');
                            res.status(500);
                            res.send(err);
                            return;
                        } else {
                            console.log('Sending ' + captionedImage);
                            res.status(200);
                            res.sendfile(captionedImage);
                        }
                    });
                }
            });
        }
    });
};

exports.initialize = function(app) {
    db.run("CREATE TABLE IF NOT EXISTS pastMemes (url TEXT, filename TEXT)");
    app.get('/meme', exports.makeMeme);

    return this;
};
