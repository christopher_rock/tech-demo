var express = require('express');
var app = express();

var geohash = require('./geohash.js').initialize(app);
var meme = require('./meme.js').initialize(app);

app.configure(function(){
  app.use(express.compress());
  app.use(express.favicon('/img/favicon.png'));
  app.use("/", express.static(__dirname + '/..'));
  app.use(express.json());
  app.use(express.urlencoded());
});

app.configure('development', function(){
  var server = app.listen(3000, function() {
    console.log('Listening on port %d', server.address().port);
  });
});

app.configure('production', function(){
  var server = app.listen(80, function() {
    console.log('Listening on port %d', server.address().port);
  });
});
