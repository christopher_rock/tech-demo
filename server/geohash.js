var moment = require('moment'),
    mtz = require('moment-timezone'),
    wolfram = require('wolfram-alpha').createClient("Q63P8K-9KPYAWL7JP", {format:'plaintext'}),
    http = require('http');

exports.getDow = function(req, res){
    var date = moment(req.params.date, "YYYY-MM-DD");

    console.log("About to make request");

    wolfram.query("dji closing price", function (err, results) {
        if (err) {
            res.status(500);
            res.send("No Dow for you!");
        }
        var toRet = {};
        for (var i in results) {
            var pod = results[i];
            if (pod.primary && pod.subpods.length >= 1) {
                toRet.dow = pod.subpods[0].text.split(' ')[0];
            }
        }
        res.status(200);
        res.send(toRet);
    });
};

exports.initialize = function(app) {
  app.get('/dow/:date', exports.getDow);

  return this;
};
